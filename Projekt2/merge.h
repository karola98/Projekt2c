#pragma once

template <class Typ>
void merge(Typ *tablica, int left, int middle, int right);

template <class Typ>
void mergeSort(Typ *tablica, int left, int right);
