#include "pch.h"
#include "test.h"
#include "merge.h"
#include "merge.cpp"
#include "quickM.h"
#include "quickM.cpp"
#include "heap.h"
#include "heap.cpp"
#include "introM.h"
#include "introM.cpp"
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
#include<fstream>
#include <cstdlib>



void test_merge(int rozmiar, double posortowane, bool odwrotnie, bool sprawdzać)
{
	const int nbOfExperiments = 100;
	std::ofstream zapis("1.txt");

	/*Wykonanie eksperymentu nbOfExperiments razy dla tablic o rozmiarze n*/

	for (int i = 0; i < nbOfExperiments; ++i)
	{
		//zapis << i << "   ";
		int n = rozmiar;

		/*
		Przygotowanie tablic o rozmiarze n:
		•wszystkie elementy tablicy losowe,
		• 25%, 50%, 75%, 95%, 99%, 99,7% początkowych elementów tablicy jest już posortowanych,
		• wszystkie elementy tablicy już posortowane ale w odwrotnej kolejności.
		*/
		int *tab = new int[n];
		int *kopia_tab = new int[n];
		for (int j = 0; j < n; j++)
		{
			tab[j] = (rand());
			kopia_tab[j] = tab[j];
		}

		int x = posortowane * n;
		if (odwrotnie)
			std::sort(tab, tab + x, std::greater<int>());
		else
			std::sort(tab, tab + x);

		int r = n - 1;

		/* Mierzenie czasu trwania jednego eksperymentu
		Poniższy fragment wykonywany jest wielokrotnie
		osobno dla każdej kombinacji algorytmu i tablicy wejściowej*/
		auto start = std::chrono::system_clock::now();
		//pojedyncze wykonanie jednego algorytmu sortowania dla jednej tablicy
		//sortowanie na kopii tablic wejściowych

		mergeSort(tab, 0, r);

		auto end = std::chrono::system_clock::now();

		std::chrono::duration<double> diff = end - start; // w sekundach https://en.cppreference.com/w/cpp/chrono/duration
		double durationTime = diff.count(); // zmierzony czas zapisać do pliku lub zagregować, zapisać liczbę badanych elementów
		zapis << durationTime << "\n";

		/*Sprawdzenie poprawności działania algorytmu */
		if (sprawdzać)
		{
			int *sorted = new int[n];
			std::copy(kopia_tab, kopia_tab + n, sorted);
			std::sort(sorted, sorted + n);
			bool ok = std::equal(sorted, sorted + n, tab, tab + n);
			if (ok) std::cout << i << "ok\n";
			delete[] sorted;
		}

		delete[] tab;
		delete[] kopia_tab;

	}

	zapis.close();
}

void test_quick(int rozmiar, double posortowane, bool odwrotnie, bool sprawdzać)
{
	const int nbOfExperiments = 100;
	std::ofstream zapis("1.txt");

	/*Wykonanie eksperymentu nbOfExperiments razy dla tablic o rozmiarze n*/

	for (int i = 0; i < nbOfExperiments; ++i)
	{
		//zapis << i << "   ";
		int n = rozmiar;

		/*
		Przygotowanie tablic o rozmiarze n:
		•wszystkie elementy tablicy losowe,
		• 25%, 50%, 75%, 95%, 99%, 99,7% początkowych elementów tablicy jest już posortowanych,
		• wszystkie elementy tablicy już posortowane ale w odwrotnej kolejności.
		*/
		int *tab = new int[n];
		int *kopia_tab = new int[n];
		for (int j = 0; j < n; j++)
		{
			tab[j] = (rand());
			kopia_tab[j] = tab[j];
		}

		int x = posortowane * n;
		if (odwrotnie)
			std::sort(tab, tab + x, std::greater<int>());
		else
			std::sort(tab, tab + x);

		int r = n - 1;

		/* Mierzenie czasu trwania jednego eksperymentu
		Poniższy fragment wykonywany jest wielokrotnie
		osobno dla każdej kombinacji algorytmu i tablicy wejściowej*/
		auto start = std::chrono::system_clock::now();
		//pojedyncze wykonanie jednego algorytmu sortowania dla jednej tablicy
		//sortowanie na kopii tablic wejściowych

		quickSortM(tab, 0, r);

		auto end = std::chrono::system_clock::now();

		std::chrono::duration<double> diff = end - start; // w sekundach https://en.cppreference.com/w/cpp/chrono/duration
		double durationTime = diff.count(); // zmierzony czas zapisać do pliku lub zagregować, zapisać liczbę badanych elementów
		zapis << durationTime << "\n";

		/*Sprawdzenie poprawności działania algorytmu */
		if (sprawdzać)
		{
			int *sorted = new int[n];
			std::copy(kopia_tab, kopia_tab + n, sorted);
			std::sort(sorted, sorted + n);
			bool ok = std::equal(sorted, sorted + n, tab, tab + n);
			if (ok) std::cout << i << "ok\n";
			delete[] sorted;
		}

		delete[] tab;
		delete[] kopia_tab;

	}

	zapis.close();
}


void test_heap(int rozmiar, double posortowane, bool odwrotnie, bool sprawdzać)
{
	const int nbOfExperiments = 100;
	std::ofstream zapis("1.txt");

	/*Wykonanie eksperymentu nbOfExperiments razy dla tablic o rozmiarze n*/

	for (int i = 0; i < nbOfExperiments; ++i)
	{
		//zapis << i << "   ";
		int n = rozmiar;

		/*
		Przygotowanie tablic o rozmiarze n:
		•wszystkie elementy tablicy losowe,
		• 25%, 50%, 75%, 95%, 99%, 99,7% początkowych elementów tablicy jest już posortowanych,
		• wszystkie elementy tablicy już posortowane ale w odwrotnej kolejności.
		*/
		int *tab = new int[n];
		int *kopia_tab = new int[n];
		for (int j = 0; j < n; j++)
		{
			tab[j] = (rand());
			kopia_tab[j] = tab[j];
		}

		int x = posortowane * n;
		if (odwrotnie)
			std::sort(tab, tab + x, std::greater<int>());
		else
			std::sort(tab, tab + x);

		int r = n - 1;

		/* Mierzenie czasu trwania jednego eksperymentu
		Poniższy fragment wykonywany jest wielokrotnie
		osobno dla każdej kombinacji algorytmu i tablicy wejściowej*/
		auto start = std::chrono::system_clock::now();
		//pojedyncze wykonanie jednego algorytmu sortowania dla jednej tablicy
		//sortowanie na kopii tablic wejściowych

		heapsort(tab, rozmiar);

		auto end = std::chrono::system_clock::now();

		std::chrono::duration<double> diff = end - start; // w sekundach https://en.cppreference.com/w/cpp/chrono/duration
		double durationTime = diff.count(); // zmierzony czas zapisać do pliku lub zagregować, zapisać liczbę badanych elementów
		zapis << durationTime << "\n";

		/*Sprawdzenie poprawności działania algorytmu */
		if (sprawdzać)
		{
			int *sorted = new int[n];
			std::copy(kopia_tab, kopia_tab + n, sorted);
			std::sort(sorted, sorted + n);
			bool ok = std::equal(sorted, sorted + n, tab, tab + n);
			if (ok) std::cout << i << "ok\n";
			delete[] sorted;
		}

		delete[] tab;
		delete[] kopia_tab;

	}

	zapis.close();
}


void test_intro(int rozmiar, double posortowane, bool odwrotnie, bool sprawdzać)
{
	const int nbOfExperiments = 100;
	std::ofstream zapis("1.txt");

	/*Wykonanie eksperymentu nbOfExperiments razy dla tablic o rozmiarze n*/

	for (int i = 0; i < nbOfExperiments; ++i)
	{
		//zapis << i << "   ";
		int n = rozmiar;

		/*
		Przygotowanie tablic o rozmiarze n:
		•wszystkie elementy tablicy losowe,
		• 25%, 50%, 75%, 95%, 99%, 99,7% początkowych elementów tablicy jest już posortowanych,
		• wszystkie elementy tablicy już posortowane ale w odwrotnej kolejności.
		*/
		int *tab = new int[n];
		int *kopia_tab = new int[n];
		for (int j = 0; j < n; j++)
		{
			tab[j] = (rand());
			kopia_tab[j] = tab[j];
		}

		int x = posortowane * n;
		if (odwrotnie)
			std::sort(tab, tab + x, std::greater<int>());
		else
			std::sort(tab, tab + x);

		int r = n - 1;

		/* Mierzenie czasu trwania jednego eksperymentu
		Poniższy fragment wykonywany jest wielokrotnie
		osobno dla każdej kombinacji algorytmu i tablicy wejściowej*/
		auto start = std::chrono::system_clock::now();
		//pojedyncze wykonanie jednego algorytmu sortowania dla jednej tablicy
		//sortowanie na kopii tablic wejściowych

		introSortM(tab, rozmiar);

		auto end = std::chrono::system_clock::now();

		std::chrono::duration<double> diff = end - start; // w sekundach https://en.cppreference.com/w/cpp/chrono/duration
		double durationTime = diff.count(); // zmierzony czas zapisać do pliku lub zagregować, zapisać liczbę badanych elementów
		zapis << durationTime << "\n";

		/*Sprawdzenie poprawności działania algorytmu */
		if (sprawdzać)
		{
			int *sorted = new int[n];
			std::copy(kopia_tab, kopia_tab + n, sorted);
			std::sort(sorted, sorted + n);
			bool ok = std::equal(sorted, sorted + n, tab, tab + n);
			if (ok) std::cout << i << "ok\n";
			delete[] sorted;
		}

		delete[] tab;
		delete[] kopia_tab;

	}

	zapis.close();
}
