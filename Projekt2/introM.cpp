#include "pch.h"
#include "introM.h"
#include "heap.h"
#include "insertion.h"
#include "insertion.cpp"
#include <iostream>

template <class Typ>
void introSortM(Typ *tablica, int rozmiar)
{
	sort(tablica, rozmiar, (int)floor(2 * log(rozmiar)));
	insertionSort(tablica, rozmiar);
}

template <class Typ>
void sort(Typ *tablica, int rozmiar, int M)
{
	if (M <= 0)
	{
		heapsort(tablica, rozmiar);
		return;
	}

	int i = 0;
	int j = rozmiar - 1;
	int x = tablica[(0 + rozmiar) / 2];
	do
	{
		while (tablica[i] < x)
			i++;

		while (tablica[j] > x)
			j--;

		if (i <= j)
		{
			std::swap(tablica[i], tablica[j]);

			i++;
			j--;
		}
	} while (i <= j);

	if (i > 9)
		sort(tablica, i, M - 1);
	if (rozmiar - 1 - i > 9)
		sort(tablica + i + 1, rozmiar - 1 - i, M - 1);
}

