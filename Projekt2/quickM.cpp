#include "pch.h"
#include "quickM.h"
#include <cmath>
#include <iostream>


template <class Typ>
static void quickSortM(Typ* tablica, int left, int right) {
	int i = left;
	int j = right;
	int pivot = tablica[(left + right) / 2];
	do
	{
		while (tablica[i] < pivot)
			i++;

		while (tablica[j] > pivot)
			j--;

		if (i <= j)
		{
			std::swap(tablica[i], tablica[j]);

			i++;
			j--;
		}
	} while (i <= j);

	if (left < j) quickSortM(tablica, left, j);

	if (right > i)	quickSortM(tablica, i, right);
}
