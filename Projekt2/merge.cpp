#include "pch.h"
#include "merge.h"
#include <iostream>
#include <cmath>


template <class Typ>
void merge(Typ *tablica, int left, int middle, int right)
{
	Typ *temp = new int[(right - left)];

	int i = left;
	int j = middle + 1;
	int k = 0;

	while (i <= middle && j <= right)
	{
		if (tablica[i] < tablica[j])
		{
			temp[k] = tablica[i];
			i++;
		}
		else
		{
			temp[k] = tablica[j];
			j++;
		}
		k++;
	}


	while (i <= middle)
	{
		temp[k] = tablica[i];
		i++;
		k++;
	}


	while (j <= right)
	{
		temp[k] = tablica[j];
		j++;
		k++;
	}

	for (int i = 0; i <= (right - left); i++)
		tablica[left + i] = temp[i];
}


template <class Typ>
void mergeSort(Typ *tablica, int left, int right)
{
	if (left < right)
	{
		int middle = (left + right) / 2;
		mergeSort<Typ>(tablica, left, middle);
		mergeSort<Typ>(tablica, middle + 1, right);
		merge<Typ>(tablica, left, middle, right);
	}

}