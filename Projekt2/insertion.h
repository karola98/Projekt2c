#pragma once
#include <iostream>

template <class Typ>
void insert(Typ *tablica, int rightIndex, Typ value);

template <class Typ>
void insertionSort(Typ *tablica, int size);
