#include "pch.h"
#include "insertion.h"

template <class Typ>
void insert(Typ *tablica, int rightIndex, Typ value)
{
	int i;
	for (i = rightIndex; i >= 0 && tablica[i] > value; i--)
	{
		tablica[i + 1] = tablica[i];
	}

	tablica[i + 1] = value;
};

template <class Typ>
void insertionSort(Typ *tablica, int size) {
	for (int i = 1; i < size; i++) {
		insert(tablica, i - 1, tablica[i]);
	}
};
