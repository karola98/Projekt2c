#include "pch.h"
#include "heap.h"
#include <iostream>
#include <cmath>

template <class Typ>
void heapify(Typ *tablica, int rozmiar, int i)
{
	int largest;
	int l = 2 * i, r = (2 * i) + 1;
	if (l <= rozmiar && tablica[l] > tablica[i])
		largest = l;
	else largest = i;
	if (r <= rozmiar && tablica[r] > tablica[largest])
		largest = r;
	if (largest != i)
	{
		std::swap(tablica[largest], tablica[i]);
		heapify(tablica, rozmiar, largest);
	}
}

template <class Typ>
void build_heap(Typ *tablica, int rozmiar)
{
	for (int i = rozmiar / 2; i > 0; --i)
		heapify(tablica - 1, rozmiar, i);
}

template <class Typ>
void heapsort(Typ *tablica, int rozmiar)
{
	build_heap(tablica, rozmiar - 1);
	for (int i = (rozmiar - 1); i > 0; --i)
	{
		std::swap(tablica[i], tablica[0]);
		heapify(tablica - 1, i, 1);
	}
}