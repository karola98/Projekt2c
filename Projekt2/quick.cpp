#include "pch.h"
#include "quick.h"
#include <cmath>
#include <iostream>


static int Partition(int* tablica, int p, int r) {
	int pivot = tablica[r];
	int i = p;

	for (int j = p; j < r; ++j)
	{
		if (tablica[j] <= pivot)
		{
			std::swap(tablica[i], tablica[j]);
			i++;
		}
	}

	tablica[r] = tablica[i];
	tablica[i] = pivot;

	return i;
}

static void quickSort(int* tablica, int p, int r) {
	if (p < r)
	{
		int q = Partition(tablica, p, r);
		quickSort(tablica, p, q - 1);
		quickSort(tablica, q + 1, r);
	}
}
