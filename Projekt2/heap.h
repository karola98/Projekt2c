#pragma once


template <class Typ>
void heapify(Typ *tablica, int rozmiar, int i);

template <class Typ>
void build_heap(Typ *tab, int rozmiar);

template <class Typ>
void heapsort(Typ *tab, int rozmiar);